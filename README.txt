//
//  README.txt
//  cards
//
//  Created by Jeremy Thompson on 2/02/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

Name: Jeremy Thompson
Email: jthompson2214@gmail.com
Time for Project: 30 hours. most of that was my own trying to get the view to scale between the 5S and the 6. I ultimately gave up and settled for the 5S
Run with the iPhone 5S simulator.
Comments or notes: has 2 and 3 card matching. the switch is disabled during peek. the peek disables all buttons until you un-peek, at which time you still have the original cards flipped up (if there were any). I believe the scoring is correct and I was able to find out how to specify precision for the average score. In addition I have animations for the flips of all the cards. Almost all of the game logic was taken out of the view controller and put into the cardMatchingClass.

You may also notice that if you go to the home screen on the simulated device there is an icon. I finally figured out what I needed to do for that so now the app has an icon. 